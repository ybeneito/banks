package fr.java.spring.microservices.banks;

import org.springframework.data.repository.CrudRepository;

public interface BanksRepository extends CrudRepository<Banks, Long> {
    
}
