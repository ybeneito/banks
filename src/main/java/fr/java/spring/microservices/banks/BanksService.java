package fr.java.spring.microservices.banks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BanksService {
    @Autowired BanksRepository repo;

    public Iterable<Banks> findAll() throws EmptyExeception{
        if(!repo.findAll().iterator().hasNext()) throw new EmptyExeception();
        
        
        return repo.findAll();
    }

    public void save(Banks b) {
        this.repo.save(b);
    }
}
