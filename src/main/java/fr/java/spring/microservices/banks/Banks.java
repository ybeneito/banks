package fr.java.spring.microservices.banks;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Banks {
    @Id
    private Long id;

    private String name;

    private String bicCountry;

    private String domesticFREtablissement;

    private String domesticFRGuichet;

    private Number postalCode;

    private String city;

    private String bankGroupName;
}
