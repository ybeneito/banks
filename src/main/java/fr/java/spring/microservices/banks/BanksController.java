package fr.java.spring.microservices.banks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BanksController {

    @Autowired BanksService bService;
    
    @GetMapping(path = "/banks")
    public Iterable<Banks> getBanks() throws EmptyExeception{
        return bService.findAll();
    }

    // Sert juste à l'initialisation
    /*
    @GetMapping(path = "/banks/add")
    public Banks addBanks(){
        Banks b = new Banks();
        b.setId(2L);
        b.setName("LCL  LANGON");
        b.setBicCountry("CRLYFRPP");
        b.setDomesticFREtablissement("30002");
        b.setDomesticFRGuichet("01836");
        b.setPostalCode(33210);
        b.setCity("LANGON");
        b.setBankGroupName("");
        this.bService.save(b);
        return b;
    }
    */
    
    
}
